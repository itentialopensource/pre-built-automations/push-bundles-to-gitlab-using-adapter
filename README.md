<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-23-2023 and will be end of life on 06-30-2024. The capabilities of this Pre-Built have been replaced by the [Itential Prebuilt Promotion Workflow Project](https://gitlab.com/itentialopensource/pre-built-automations/prebuilt-promotion)

<!-- Update the below line with your artifact name -->
# Push Bundles to Gitlab using Adapter

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [Push Bundles to Gitlab using Adapter](#push-bundles-to-gitlab-using-adapter)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Requirements](#requirements)
  - [Features](#features)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!-- <!--  -->
The **Push Bundles to Gitlab** pre-built takes an Admin Essentials installed artifact and creates a new project in GitLab using the up-to-date artifact bundle.
If the project and branch already exists in the specified GitLab group, it will create a new branch and open a merge request (MR) in GitLab with any changes made in the lab environment.

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`
* App-Artifacts
  * `6.1.16-2021.1.2`
* [GitLab Adapter](https://gitlab.com/itentialopensource/adapters/devops-netops/adapter-gitlab)

## Requirements

This artifact requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
* Artifact installed in Admin Essentials
* GitLab environment URL to a specific group
* GitLab private access token
* Argo (Itential open source Docker Hub image) access via GitLab pipeline

## Features

The main benefits and features of the artifact are outlined below.

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
* Supports internal Git repos with no internet access (Argo docker image can be digested offline).
* Automatically create repo and branch.
* Automatically create MR when repo and branch exist.
* Allows user to perform rediscovery of an installed artifact (where new components were added).
* Updates Admin Essentials with the latest list of components.
* Adds the current IAP user whoami username to the MR description for the MR reviewer.
* Checks-in all necessary files for CI/CD (including auto artifact.json generator script).
* Helps to handle "Artifact-As-Code" with version control, MR, and code-promotion procedures.


<!-- ## Future Enhancements -->

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

To install this pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section. 
* The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button.


<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the artifact:

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->

1. In Automation Catalog, find the **Push Bundles to Gitlab** entry.
2. Fill out the form with the appropriate values.
3. Continue with all manual tasks in the workflow.

Form Inputs
1. Adapter Name - Adapter name configured with user token
2. GitLab Project Name - Project name in GitLab to Update/Create
3. GitLab Group Path - Group path for GitLab subgroup
4. Re-discover - Perform re0discover
5. MR Type - Type of MR (patch/minor/major)
6. Commit Message -  Commit message to add for commit tasks
7. Target Branch - Traget branch to set for MR


