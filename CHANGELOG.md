
## 0.0.13 [12-04-2023]

* Removes image tags

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!20

---

## 0.0.12 [10-19-2023]

* Updates package.json for version fix

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!19

---

## 0.0.11-2022.1.0 [07-08-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!8

---

## 0.0.11 [07-08-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!8

---

## 0.0.10 [06-07-2022]

* Re certified for 2021.2

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!7

---

## 0.0.9 [03-23-2022]

* update version in package.json [skip ci]

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!6

---

## 0.0.8-2021.1.0 [07-28-2021]

* Update bundles/transformations/updatePackageJSONGitlab.json,...

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!3

---

## 0.0.8 [07-28-2021]

* Update bundles/transformations/updatePackageJSONGitlab.json,...

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!3

---

## 0.0.7 [07-02-2021]

* Update README.md, package.json files

See merge request itentialopensource/pre-built-automations/push-bundles-to-gitlab-using-adapter!2

---

## 0.0.6 [04-19-2021]

* last prebuilt updates before publishing

See merge request itentialopensource/pre-built-automations/staging/push-bundles-to-gitlab-using-adapter!1

---

## 0.0.5 [04-14-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.4 [04-13-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [04-13-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [04-13-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
